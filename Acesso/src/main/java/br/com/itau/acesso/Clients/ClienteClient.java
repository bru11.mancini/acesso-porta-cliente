package br.com.itau.acesso.Clients;

import br.com.itau.acesso.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)

public interface ClienteClient {

    @GetMapping("/cliente/{idCliente}")
    Cliente getClientePorId(@PathVariable int idCliente);
}
