package br.com.itau.acesso.Clients;

import br.com.itau.acesso.Decoder.ClienteClientDecoder;
import br.com.itau.acesso.Fallback.ClienteClientFallback;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getErroDecoder(){

        return new ClienteClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), RetryableException.class)
                .build();
        return Resilience4jFeign.builder(feignDecorators);
    }
}
