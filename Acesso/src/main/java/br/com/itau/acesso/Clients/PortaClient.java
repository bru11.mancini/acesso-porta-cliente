package br.com.itau.acesso.Clients;


import br.com.itau.acesso.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)

public interface PortaClient {

    @GetMapping("/porta/{idPorta}")
    Porta consultarPortaPorId(@PathVariable int idPorta);
}
