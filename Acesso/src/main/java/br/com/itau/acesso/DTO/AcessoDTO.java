package br.com.itau.acesso.DTO;

public class AcessoDTO {

    private int porta_id;

    private int cliente_id;

    public AcessoDTO() {
    }

    public AcessoDTO(int porta_id, int cliente_id) {
        this.porta_id = porta_id;
        this.cliente_id = cliente_id;
    }

    public int getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(int porta_id) {
        this.porta_id = porta_id;
    }

    public int getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
    }
}
