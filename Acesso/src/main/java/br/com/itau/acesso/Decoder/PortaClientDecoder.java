package br.com.itau.acesso.Decoder;

import br.com.itau.acesso.exceptions.PortaNotFoudException;
import feign.Response;
import feign.codec.ErrorDecoder;


public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400){
            return new PortaNotFoudException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
