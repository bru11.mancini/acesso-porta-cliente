package br.com.itau.acesso.Fallback;

import br.com.itau.acesso.Clients.ClienteClient;
import br.com.itau.acesso.exceptions.ClienteOfflineException;
import br.com.itau.acesso.models.Cliente;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente getClientePorId(int idCliente){
        throw new ClienteOfflineException();
    }
}
