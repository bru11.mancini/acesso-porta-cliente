package br.com.itau.acesso.Fallback;

import br.com.itau.acesso.Clients.ClienteClient;
import br.com.itau.acesso.Clients.PortaClient;
import br.com.itau.acesso.exceptions.ClienteOfflineException;
import br.com.itau.acesso.exceptions.PortaOfflineException;
import br.com.itau.acesso.models.Cliente;
import br.com.itau.acesso.models.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta consultarPortaPorId(int idPorta){
        throw new PortaOfflineException();
    }
}
