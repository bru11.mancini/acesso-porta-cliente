package br.com.itau.acesso.Producer;

public class AcessoKafka {

    private boolean temAcesso;

    public AcessoKafka() {
    }

    public AcessoKafka(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }

    public boolean isTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }
}
