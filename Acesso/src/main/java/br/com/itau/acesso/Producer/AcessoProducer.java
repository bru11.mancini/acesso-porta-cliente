package br.com.itau.acesso.Producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, AcessoKafka> producer;

    public void enviarAoKafka(AcessoKafka acessoKafka){
        producer.send("spec3-bruno-mancini-1", acessoKafka);
    }
}
