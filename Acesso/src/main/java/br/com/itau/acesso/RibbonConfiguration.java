package br.com.itau.acesso;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    @Bean
    public IRule getRule(){
        return new RandomRule();
        //exemplo só pra mostrar que o balanceador usa a regra que eu quiser. No caso, ele vai mandar de forma ramdomica.
    }
}
