package br.com.itau.acesso.controllers;

import br.com.itau.acesso.DTO.AcessoDTO;
import br.com.itau.acesso.repositories.AcessoId;
import br.com.itau.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController

public class AcessoController {


    private AcessoDTO acessoDTO;

    @Autowired
    private AcessoService acessoService;

    @PostMapping("/acesso")
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoDTO cadastrarAcesso(@RequestBody AcessoDTO acessoDTO){
            acessoService.cadastrarAcesso(acessoDTO.getCliente_id(), acessoDTO.getPorta_id());
            return acessoDTO;
            //fiz diferente de propósito, só pra lembrar que tem N formas de voltar.
    }


    @GetMapping("/acesso/{idCliente}/{idPorta}")
    @ResponseStatus(HttpStatus.OK)
    public AcessoId exibirAcesso (@PathVariable(name = "idCliente", required = true) Integer idCliente,
                                @PathVariable(name = "idPorta", required = true) Integer idPorta){

        try {
            AcessoId acessoIdObjeto = acessoService.buscarAcesso(idCliente, idPorta);
            return acessoIdObjeto;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/acesso/{idCliente}/{idPorta}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public AcessoId apagarAcesso (@PathVariable(name = "idCliente", required = true) Integer idCliente,
                                @PathVariable(name = "idPorta", required = true) Integer idPorta){

        try {
            AcessoId acessoObjeto = acessoService.apagarAcesso(idCliente, idPorta);
            return acessoObjeto;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
