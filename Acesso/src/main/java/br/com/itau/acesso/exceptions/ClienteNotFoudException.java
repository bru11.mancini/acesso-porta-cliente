package br.com.itau.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não encontrado. Mensagem vinda da ClienteNotFoundException")
public class ClienteNotFoudException extends RuntimeException {
}
