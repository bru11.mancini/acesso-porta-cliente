package br.com.itau.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta não encontrada. Mensagem vinda da PortaNotFoundException")
public class PortaNotFoudException extends RuntimeException {
}
