package br.com.itau.acesso.models;

import br.com.itau.acesso.repositories.AcessoId;

import javax.persistence.*;

@Entity
public class Acesso {

    @EmbeddedId
    @Id
    @Column(unique = true)
    private AcessoId id;

    public Acesso() {
    }

    public Acesso(AcessoId id) {
        this.id = id;
    }

    public AcessoId getId() {
        return id;
    }

    public void setId(AcessoId id) {
        this.id = id;
    }
}
