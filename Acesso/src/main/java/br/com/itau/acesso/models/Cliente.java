package br.com.itau.acesso.models;

public class Cliente {

    private int id;

    private String nome;

    public Cliente() {
    }

    public Cliente(int clienteId, String name) {
        this.id = clienteId;
        this.nome = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    }
