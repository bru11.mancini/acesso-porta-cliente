package br.com.itau.acesso.models;

public class Porta {

    private int id;

    private String andar;

    private String sala;


    public Porta() {
    }

    public Porta(int id, String andar, String sala) {
        this.id = id;
        this.andar = andar;
        this.sala = sala;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
    }
