package br.com.itau.acesso.repositories;

import br.com.itau.acesso.models.Acesso;

import org.springframework.data.repository.CrudRepository;


public interface AcessoRepository extends CrudRepository<Acesso, AcessoId> {

}

