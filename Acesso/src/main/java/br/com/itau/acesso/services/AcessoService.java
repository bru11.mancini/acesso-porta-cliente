package br.com.itau.acesso.services;

import br.com.itau.acesso.Clients.ClienteClient;
import br.com.itau.acesso.Clients.PortaClient;
import br.com.itau.acesso.Producer.AcessoKafka;
import br.com.itau.acesso.Producer.AcessoProducer;
import br.com.itau.acesso.models.Acesso;

import br.com.itau.acesso.models.Cliente;
import br.com.itau.acesso.models.Porta;
import br.com.itau.acesso.repositories.AcessoId;
import br.com.itau.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {
    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    PortaClient portaClient;

    @Autowired
    private AcessoProducer acessoProducer;


    private AcessoKafka acessoKafka;

    public Acesso cadastrarAcesso(int idCliente, int idPorta){
            Cliente cliente = clienteClient.getClientePorId(idCliente);
            Porta porta = portaClient.consultarPortaPorId(idPorta);

            AcessoId acessoId = new AcessoId(idPorta, idCliente);

            Acesso acessoObjeto = new Acesso();

            acessoObjeto.setId(acessoId);
            return acessoObjeto = acessoRepository.save(acessoObjeto);
    }

    public AcessoId buscarAcesso(int idCliente, int idPorta){
        AcessoId acessoId = new AcessoId(idPorta, idCliente);
        Optional<Acesso> optionalAcesso = acessoRepository.findById(acessoId);
        AcessoKafka acessoKafka = new AcessoKafka();
        if (optionalAcesso.isPresent()){
            acessoKafka.setTemAcesso(true);
            acessoProducer.enviarAoKafka(acessoKafka);
            return acessoId;
        }else {
            acessoKafka.setTemAcesso(false);
            acessoProducer.enviarAoKafka(acessoKafka);
        }

        throw new RuntimeException("O acesso não foi encontrado na base de acessos. Essa mensagem voltou na classe de busca!");
    }

    public AcessoId apagarAcesso(int idCliente, int idPorta){
        AcessoId acessoId = buscarAcesso(idCliente, idPorta);
        acessoRepository.deleteById(acessoId);
        return acessoId;

    }

}
