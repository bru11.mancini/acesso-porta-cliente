package br.com.itau.Cliente.controllers;

import br.com.itau.Cliente.models.Cliente;
import br.com.itau.Cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping("/cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody Cliente cliente){
        return clienteService.salvarCliente(cliente);
    }

    @GetMapping("/cliente/{idCliente}")
    public Cliente exibirCliente (@PathVariable(name = "idCliente", required = false) Integer idCliente){
        try {
            Cliente clienteObjeto = clienteService.buscarCliente(idCliente);
            return clienteObjeto;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
