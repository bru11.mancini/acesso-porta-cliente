package br.com.itau.Porta.controllers;

import br.com.itau.Porta.models.Porta;
import br.com.itau.Porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping("/porta")
    @ResponseStatus(HttpStatus.CREATED)
    public Porta cadastrarCliente(@RequestBody Porta porta){
        return portaService.salvarPorta(porta);
    }

    @GetMapping("/porta/{idPorta}")
    public Porta exibirCliente (@PathVariable(name = "idPorta", required = false) Integer idPorta){
        try {
            Porta portaObjeto = portaService.buscarPorta(idPorta);
            return portaObjeto;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
