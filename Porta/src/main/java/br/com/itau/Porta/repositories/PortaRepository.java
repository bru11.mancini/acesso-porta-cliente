package br.com.itau.Porta.repositories;

import br.com.itau.Porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
