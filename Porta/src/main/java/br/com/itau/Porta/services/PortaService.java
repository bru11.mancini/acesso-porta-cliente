package br.com.itau.Porta.services;

import br.com.itau.Porta.models.Porta;
import br.com.itau.Porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta salvarPorta(Porta porta){
        Porta portaObjeto = portaRepository.save(porta);
        return portaObjeto;
    }

    public Porta buscarPorta(int idPorta){
        Optional<Porta> optionalPorta = portaRepository.findById(idPorta);
        if (optionalPorta.isPresent()){
            return optionalPorta.get();
        }
        throw new RuntimeException("A porta não foi encontrada!");
    }

}
