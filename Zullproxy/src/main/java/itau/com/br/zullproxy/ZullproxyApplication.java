package itau.com.br.zullproxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZullproxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZullproxyApplication.class, args);
	}

}
