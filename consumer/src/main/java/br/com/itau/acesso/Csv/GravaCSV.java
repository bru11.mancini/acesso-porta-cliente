package br.com.itau.acesso.Csv;

import br.com.itau.acesso.Producer.AcessoKafka;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class GravaCSV {

    @Autowired
    AcessoKafka acessoKafka;

    public void gravarArquivo(AcessoKafka acessoKafka) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        List<AcessoKafka> acessoKafkaList = new ArrayList<>();
        acessoKafkaList.add(new AcessoKafka(acessoKafka.isTemAcesso()));

        Writer writer = Files.newBufferedWriter(Paths.get("/home/a2w/IdeaProjects/acesso-portas/status.csv"));
        StatefulBeanToCsv<AcessoKafka> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(acessoKafkaList);
        writer.flush();
        writer.close();
        System.out.println("Carregou arquivo");

    }

    public void chamaCSV(AcessoKafka acessoKafka) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        gravarArquivo(acessoKafka);
    }



}
