package br.com.itau.acesso.Producer;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AcessoKafka {

    private boolean temAcesso;

    public AcessoKafka() {
    }

    public AcessoKafka(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }

    public boolean isTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }
}
