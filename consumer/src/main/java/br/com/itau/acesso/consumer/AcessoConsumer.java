package br.com.itau.acesso.consumer;

import br.com.itau.acesso.Csv.GravaCSV;
import br.com.itau.acesso.Producer.AcessoKafka;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AcessoConsumer {


    @KafkaListener(topics = "spec3-bruno-mancini-1", groupId = "Mancini-teste")
    public void receber(@Payload AcessoKafka acessoKafka) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        System.out.println("O acesso dele está " + acessoKafka.isTemAcesso());
        GravaCSV gravaCSV = new GravaCSV();
        gravaCSV.chamaCSV(acessoKafka);
    }

}
